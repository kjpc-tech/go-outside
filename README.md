# Go Outside website

---
## KJ PC | [kjpc.tech](https://kjpc.tech/) | [kyle@kjpc.tech](mailto:kyle@kjpc.tech)
---

[Django](https://www.djangoproject.com/) powered website that helps people go outside. Uses Python 3 and Django 2.0.

## Motivation
I was bored and I wanted to learn the basics of the [Django REST Framework](http://www.django-rest-framework.org/). I also wanted to use the [GeoDjango](https://docs.djangoproject.com/en/2.0/ref/contrib/gis/) module.

### Suggested Dependencies
* binutils (https://docs.djangoproject.com/en/2.0/ref/contrib/gis/install/geolibs/)
* libproj-dev (https://docs.djangoproject.com/en/2.0/ref/contrib/gis/install/geolibs/)
* gdal-bin (https://docs.djangoproject.com/en/2.0/ref/contrib/gis/install/geolibs/)
* sqlite
* libsqlite3-mod-spatialite (https://docs.djangoproject.com/en/2.0/ref/contrib/gis/install/spatialite/)
* redis
* memcached
* Firefox (for automated testing)

## Installation
1. Clone repository
2. Install [invoke](http://www.pyinvoke.org/index.html)
    * `pip3 install invoke`
3. Change directories to project root
    * `cd /path/to/cloned/project/`
4. Install database dependencies
    * `apt install binutils libproj-dev gdal-bin` ([Django Docs](https://docs.djangoproject.com/en/2.0/ref/contrib/gis/install/geolibs/))
    * `apt install libsqlite3-mod-spatialite` ([Django Docs](https://docs.djangoproject.com/en/2.0/ref/contrib/gis/install/spatialite/))
5. Change `SPATIALITE_LIBRARY_PATH` setting
    * Run `dpkg-query -L libsqlite3-mod-spatialite` to list files installed by `libsqlite3-mod-spatialite`
    * Copy the path for `mod_spatialite.so` to the `SPATIALITE_LIBRARY_PATH` setting in Django ([Django Docs](https://docs.djangoproject.com/en/2.0/ref/contrib/gis/install/spatialite/))
6. Do initial setup
    * `invoke setup`
7. Run development server
    * `invoke runserver`
8. Run Celery workers
    * `invoke runcelery`

Note that these installation instructions may not work on your system. They are meant to be a general guideline and help in getting the application installed.

### Invoke Commands
Invoke is used to make life easier.

The following commands are available from the root directory:
* `setup` or `setup --no-debug`
* `check`
* `test` or `test --warnings`
* `makemigrations`
* `migrate`
* `createsuperuser`
* `runcelery`
* `runserver`

Run a command with `invoke <command>`.

Get command information with `invoke <command> -h`.

List available commands with `invoke -l`.

### Adding More Places
1) Create an [issue](https://gitlab.com/kjpc-tech/go-outside/issues) with a link to a GeoJSON file from [ArcGIS Hub](https://hub.arcgis.com/).

**OR**

2) Create a [merge request](https://gitlab.com/kjpc-tech/go-outside/merge_requests) with a properly formatted fixture containing the new information.
  * Dump data with `python manage.py dumpdata places.PlaceSource places.PlaceSourceInterfaceMapping places.PlaceSourceInterface --indent 2 --format yaml -o output-file.yaml`
  * Reformat to only include new information.

### License
[MIT License](LICENSE)

#### Screenshot
![](go-outside-screenshot.png)
