import unittest

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.conf import settings

if settings.RUN_SELENIUM_TESTS:
    from selenium.webdriver.firefox.webdriver import WebDriver
    from selenium.webdriver.common.keys import Keys
    from selenium.webdriver.support.ui import Select
    from selenium.common.exceptions import NoSuchElementException


@unittest.skipUnless(settings.RUN_SELENIUM_TESTS, "RUN_SELENIUM_TESTS is False")
class MySeleniumTestCase(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def assertElementDoesExist(self, css_selector):
        self.assertIsNotNone(self.selenium.find_element_by_css_selector(css_selector))

    def assertElementDoesntExist(self, css_selector):
        with self.assertRaises(NoSuchElementException):
            self.selenium.find_element_by_css_selector(css_selector)

    def clear_input_with_backspace(self, input_name):
        input_element = self.selenium.find_element_by_name(input_name)
        while len(input_element.get_attribute('value')):
            input_element.send_keys(Keys.BACKSPACE)

    def update_input_value(self, input_name, input_value):
        input_element = self.selenium.find_element_by_name(input_name)
        input_element.clear()
        input_element.send_keys(str(input_value))

    def choose_select_option(self, select_name, option_value):
        select = Select(self.selenium.find_element_by_name(select_name))
        select.select_by_value(str(option_value))
