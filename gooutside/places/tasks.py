import re

from django.contrib.gis.gdal import SpatialReference, CoordTransform
from django.contrib.gis.geos import Point

from celery.decorators import task

import requests

from .models import PlaceCategory, PlaceAttribute, Place, PlaceSourceInterface


@task(name="pull_and_process_places")
def pull_and_process_places(placesourceinterface_pk):
    """
    Task that generates Place objects from geojson file.

    Takes in a PlaceSourceInterface object. Pulls a geojson
    file from the Internet and creates/updates Places in the
    local database. The PlaceSourceInterface object maps
    the geojson file into our representation.
    """
    print("Running task.")

    try:
        placesourceinterfaceObj = PlaceSourceInterface.objects.get(pk=placesourceinterface_pk)
    except PlaceSourceInterface.DoesNotExist:
        placesourceinterfaceObj = None

    if placesourceinterfaceObj is not None:
        # get geojson from Internet
        response = requests.get(placesourceinterfaceObj.json_url)
        if response.status_code == 200:
            geojson = response.json()

            # make coordinate transformer
            coord_input_type = placesourceinterfaceObj.coord_type.evaluate_mapping(geojson)
            coord_input_type_match = re.search(r'(\d+)', coord_input_type)
            if coord_input_type_match is not None:
                coord_input_type = int(coord_input_type_match.group(1))
            coord_output_type = 4326
            coord_transformer = CoordTransform(
                SpatialReference(coord_input_type),
                SpatialReference(coord_output_type),
            )

            # get list of places
            place_list = placesourceinterfaceObj.place_list.evaluate_mapping(geojson)

            # go through list of places
            for place in place_list:

                # is the place public ?
                public = placesourceinterfaceObj.public.evaluate_mapping(place)

                if public:  # we only care about public places
                    # place name
                    name_primary = placesourceinterfaceObj.name_primary.evaluate_mapping(place)
                    if placesourceinterfaceObj.name_secondary is not None:
                        name_secondary = placesourceinterfaceObj.name_secondary.evaluate_mapping(place)
                    else:
                        name_secondary = ""

                    # place category
                    categories = []
                    for category_mapping in placesourceinterfaceObj.category_mappings.all():
                        category = category_mapping.evaluate_mapping(place)
                        if category:
                            categories.append(category)

                    # place location
                    location = placesourceinterfaceObj.location.evaluate_mapping(place)
                    location_lat = None
                    location_lng = None
                    if placesourceinterfaceObj.location_lat is not None and placesourceinterfaceObj.location_lng is not None:
                        location_lat = placesourceinterfaceObj.location_lat.evaluate_mapping(place)
                        location_lng = placesourceinterfaceObj.location_lng.evaluate_mapping(place)

                    # place attributes
                    attributes = []
                    for attribute_mapping in placesourceinterfaceObj.attribute_mappings.all():
                        attribute = attribute_mapping.evaluate_mapping(place)
                        if attribute:
                            attributes.append(attribute)

                    # place identification
                    source_identification = placesourceinterfaceObj.source_identification.evaluate_mapping(place)

                    # get existing Place or create new Place
                    try:
                        placeObj = Place.objects.get(
                            source=placesourceinterfaceObj.source,
                            source_identification=source_identification,
                        )
                    except Place.DoesNotExist:
                        placeObj = Place(
                            source=placesourceinterfaceObj.source,
                            source_identification=source_identification,
                        )

                    # set Place name
                    placeObj.name = name_primary or name_secondary

                    # set Place category
                    if len(categories) > 0:
                        place_category = PlaceCategory.objects.get_or_create(name=categories[0])[0]
                    else:
                        place_category = PlaceCategory.objects.get_or_create(name="Other")[0]
                    placeObj.category = place_category

                    # set Place location
                    if location_lat is not None and location_lng is not None:
                        # use lat and lng if provided
                        place_location = Point(x=float(location_lng), y=float(location_lat), srid=coord_output_type)
                    else:
                        place_location = Point(x=float(location[0]), y=float(location[1]), srid=coord_input_type)
                        # transform point to lat, lng
                        place_location.transform(coord_transformer)
                    placeObj.location = place_location

                    # set Place state
                    placeObj.state = placesourceinterfaceObj.state

                    placeObj.save()

                    # add attributes to Place
                    for attribute in attributes:
                        placeObj.attributes.add(PlaceAttribute.objects.get_or_create(name=attribute)[0])

    print("Task finished.")
