from django.contrib.gis.db import models

from localflavor.us.models import USStateField


class PlaceCategory(models.Model):
    name = models.CharField(max_length=128, unique=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class PlaceAttribute(models.Model):
    name = models.CharField(max_length=128, unique=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class PlaceSource(models.Model):
    name = models.CharField(max_length=128)
    description = models.CharField(max_length=255, blank=True)
    url = models.URLField(blank=True, null=True)

    class Meta:
        ordering = ['name']
        unique_together = [('name', 'url')]

    def __str__(self):
        return self.name


class Place(models.Model):
    name = models.CharField(max_length=128)
    category = models.ForeignKey(PlaceCategory, on_delete=models.CASCADE)

    location = models.PointField()
    state = USStateField()

    attributes = models.ManyToManyField(PlaceAttribute, blank=True)

    source = models.ForeignKey(PlaceSource, on_delete=models.CASCADE)
    source_identification = models.CharField(max_length=64, blank=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['state', 'name']
        unique_together = [('source', 'source_identification')]

    def __str__(self):
        return self.name


class PlaceSourceInterfaceMapping(models.Model):
    key = models.CharField(max_length=255, help_text="feature['a']['b'] becomes a__b")
    rule = models.CharField(max_length=255, help_text='lambda x: [fill this in]')
    success = models.CharField(max_length=128, blank=True)

    def __str__(self):
        return f"Mapping key: {self.key}; rule: {self.rule}"

    def get_key_str(self):
        return "".join([f"['{k}']" for k in self.key.split('__')])

    def get_rule(self):
        return lambda x: eval(self.rule)

    def evaluate_mapping(self, json_object):
        try:
            rule_function = self.get_rule()
            raw_value = eval(f"json_object{self.get_key_str()}")
            processed_value = rule_function(raw_value)
            if self.success:
                if processed_value:
                    return self.success
                else:
                    return ""
            else:
                return processed_value
        except:
            return ""


class PlaceSourceInterface(models.Model):
    source = models.OneToOneField(PlaceSource, on_delete=models.CASCADE)

    json_url = models.URLField()

    coord_type = models.OneToOneField(PlaceSourceInterfaceMapping, on_delete=models.CASCADE, related_name='coord_type')

    place_list = models.OneToOneField(PlaceSourceInterfaceMapping, on_delete=models.CASCADE, related_name='place_list')

    name_primary = models.OneToOneField(PlaceSourceInterfaceMapping, on_delete=models.CASCADE, related_name='name_primary')
    name_secondary = models.OneToOneField(PlaceSourceInterfaceMapping, on_delete=models.SET_NULL, blank=True, null=True, related_name='name_secondary')

    category_mappings = models.ManyToManyField(PlaceSourceInterfaceMapping, related_name='category_mappings')

    public = models.OneToOneField(PlaceSourceInterfaceMapping, on_delete=models.CASCADE, related_name='public')

    location = models.OneToOneField(PlaceSourceInterfaceMapping, on_delete=models.CASCADE, related_name='location')
    location_lat = models.OneToOneField(PlaceSourceInterfaceMapping, on_delete=models.CASCADE, blank=True, null=True, related_name='location_lat')
    location_lng = models.OneToOneField(PlaceSourceInterfaceMapping, on_delete=models.CASCADE, blank=True, null=True, related_name='location_lng')

    state = USStateField()

    attribute_mappings = models.ManyToManyField(PlaceSourceInterfaceMapping, blank=True, related_name='attribute_mappings')

    source_identification = models.OneToOneField(PlaceSourceInterfaceMapping, on_delete=models.CASCADE, related_name='source_identification')

    def __str__(self):
        return f"Interface for {self.source}"
