from django_filters import rest_framework as filters

from .models import Place


class PlaceFilter(filters.FilterSet):
    class Meta:
        model = Place
        fields = ('category', 'attributes', 'state')
