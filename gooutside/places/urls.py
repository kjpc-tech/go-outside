from django.urls import path

from rest_framework.urlpatterns import format_suffix_patterns

from . import views


app_name = 'places'

urlpatterns = [
    path('', views.PlacesView.as_view(), name='places'),
    path('categories/', views.PlaceCategoryList.as_view(), name='category_list'),
    path('attributes/', views.PlaceAttributeList.as_view(), name='attribute_list'),
    path('sources/', views.PlaceSourceList.as_view(), name='source_list'),
    path('places/', views.PlaceList.as_view(), name='place_list'),
]


urlpatterns = format_suffix_patterns(urlpatterns)
