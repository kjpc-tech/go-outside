const MAP_DEFAULT_CENTER = {lat: 40.0109, lng: -96.2312};  // centerish of US
const MAP_DEFAULT_ZOOM = 4;

const vue = new Vue({
  el: "#app",
  delimiters: ['((', '))'],

  data: {
    categories: [],
    categories_by_key: {},
    category: 'all',
    category_list_url: "",

    attributes: [],
    attributes_by_key: {},
    checked_attributes: [],
    attribute_list_url: "",

    state: 'all',

    sources: [],
    sources_by_key: {},
    source_list_url: "",

    search: "",
    search_timeout: null,

    places: [],
    place_list_url: "",

    map: null,
    markers: [],
    current_place_index: null,
  },

  watch: {
    category: 'load_places',
    checked_attributes: 'load_places',
    state: 'load_places',
    search: function() {
      if (this.search_timeout) {
        clearTimeout(this.search_timeout);
      }
      this.search_timeout = setTimeout(this.load_places, 333);
    },

    places: 'annotate_map',
  },

  mounted: function() {
    var self = this;

    this.category_list_url = this.$refs.category_list_url.getAttribute('data-default');
    this.load_categories();

    this.attribute_list_url = this.$refs.attribute_list_url.getAttribute('data-default');
    this.load_attributes();

    this.source_list_url = this.$refs.source_list_url.getAttribute('data-default');
    this.load_sources();

    this.place_list_url = this.$refs.place_list_url.getAttribute('data-default');
    this.load_places();
  },

  methods: {
    load_categories: function() {
      this.$http.get(this.category_list_url).then(response => {
        // success
        this.categories = response.data;
        this.categories_by_key = {};
        for (let i = 0; i < this.categories.length; i++) {
          this.categories_by_key[this.categories[i].id] = this.categories[i];
        }
      }, response => {
        // error
        console.error("Loading categories failed.", response);
      });
    },

    load_attributes: function() {
      this.$http.get(this.attribute_list_url).then(response => {
        // success
        this.attributes = response.data;
        this.attributes_by_key = {};
        for (let i = 0; i < this.attributes.length; i++) {
          this.attributes_by_key[this.attributes[i].id] = this.attributes[i];
        }
      }, response => {
        // error
        console.error("Loading attributes failed.", response);
      });
    },

    load_sources: function() {
      this.$http.get(this.source_list_url).then(response => {
        // success
        this.sources = response.data;
        this.sources_by_key = {};
        for (let i = 0; i < this.sources.length; i++) {
          this.sources_by_key[this.sources[i].id] = this.sources[i];
        }
      }, response => {
        // error
        console.error("Loading sources failed.", response);
      });
    },

    load_places: function() {
      let params = {};
      if (this.category != 'all') {
        params.category = this.category;
      }
      if (this.checked_attributes.length > 0) {
        params.attributes = this.checked_attributes;
      }
      if (this.state != 'all') {
        params.state = this.state;
      }
      if (this.search) {
        params.search = this.search;
      }
      this.$http.get(this.place_list_url, {
        params: params,
        before: function(request) {
          if (this.places_request) {
            this.places_request.abort();
          }
          this.places_request = request;
        },
      }).then(response => {
        // success
        this.places_request = null;
        this.places = response.data;
      }, response => {
        // error
        this.places_request = null;
        console.error("Loading places failed.", response);
      });
    },

    adjust_attributes: function(e) {
      if (e.target.checked) {
        this.checked_attributes.push(parseInt(e.target.value));
      } else {
        this.checked_attributes.splice(this.checked_attributes.indexOf(parseInt(e.target.value)), 1);
      }
    },

    setup_map: function() {
      this.map = new google.maps.Map(this.$refs.map, {
        center: MAP_DEFAULT_CENTER,
        zoom: MAP_DEFAULT_ZOOM,
      });

      this.annotate_map();
    },

    annotate_map: function() {
      var self = this;

      if (this.map !== null) {
        // clear all markers
        for (let marker = 0; marker < this.markers.length; marker++) {
          this.markers[marker].setMap(null);
        }
        this.markers = [];

        var map_bounds = new google.maps.LatLngBounds();

        // re-add markers
        for (let place = 0; place < this.places.length; place++) {
          let marker_position = this.places[place].location;
          let marker = new google.maps.Marker({
            map: this.map,
            position: marker_position,
            title: this.places[place].name,
          });
          marker.place_index = place;
          marker.addListener('click', function() {
            self.place_clicked(this);
          });
          this.markers.push(marker);
          map_bounds.extend(marker_position);
        }

        if (!map_bounds.isEmpty()) {
          // fit map to places
          this.map.fitBounds(map_bounds);
        } else {
          this.map.setCenter(MAP_DEFAULT_CENTER);
          this.map.setZoom(MAP_DEFAULT_ZOOM);
        }

        // don't get too close to the map
        if (this.map.getZoom() > 10) {
          this.map.setZoom(10);
        }
      }
    },

    place_clicked: function(marker) {
      this.current_place_index = marker.place_index;
      $("#place_info").modal('show', true);
    },
  },
});


function init_map() {
  vue.setup_map();
}
