from django.conf import settings
from django.views.generic import TemplateView

from rest_framework.filters import SearchFilter
from rest_framework import generics

from django_filters.rest_framework import DjangoFilterBackend

from localflavor.us.us_states import STATE_CHOICES

from .models import PlaceCategory, PlaceAttribute, PlaceSource, Place
from .serializers import (
    PlaceCategorySerializer, PlaceAttributeSerializer,
    PlaceSourceSerializer, PlaceSerializer,
)
from .filters import PlaceFilter


class PlacesView(TemplateView):
    template_name = 'places/places.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['states'] = STATE_CHOICES

        context['google_maps_api_key'] = getattr(settings, 'GOOGLE_MAPS_API_KEY', '')

        return context


class PlaceCategoryList(generics.ListAPIView):
    queryset = PlaceCategory.objects.all()
    serializer_class = PlaceCategorySerializer


class PlaceAttributeList(generics.ListAPIView):
    queryset = PlaceAttribute.objects.all()
    serializer_class = PlaceAttributeSerializer


class PlaceSourceList(generics.ListAPIView):
    queryset = PlaceSource.objects.all()
    serializer_class = PlaceSourceSerializer


class PlaceList(generics.ListAPIView):
    queryset = Place.objects.all()
    serializer_class = PlaceSerializer
    filter_class = PlaceFilter
    filter_backends = (SearchFilter, DjangoFilterBackend)
    search_fields = ('name',)

    def filter_queryset(self, queryset):
        filtered = super().filter_queryset(queryset)

        # TODO https://stackoverflow.com/a/8637972/5286674
        if 'attributes[]' in self.request.query_params:
            filtered = filtered.filter(attributes__in=self.request.query_params.getlist('attributes[]'))

        return filtered
