from django.contrib import admin


from .models import (
    PlaceCategory, PlaceAttribute, PlaceSource, Place,
    PlaceSourceInterfaceMapping, PlaceSourceInterface,
)
from .tasks import pull_and_process_places


admin.site.register(PlaceCategory)
admin.site.register(PlaceAttribute)
admin.site.register(PlaceSource)


@admin.register(Place)
class PlaceAdmin(admin.ModelAdmin):
    list_display = ['name', 'state', 'category', 'source']
    list_filter = ['category', 'source', 'state']
    search_fields = ['name']


admin.site.register(PlaceSourceInterfaceMapping)


@admin.register(PlaceSourceInterface)
class PlaceSourceInterfaceAdmin(admin.ModelAdmin):
    list_display = ['source']
    actions = ['start_pull_places_task']

    def start_pull_places_task(self, request, queryset):
        for placesourceinterfaceObj in queryset:
            pull_and_process_places.delay(placesourceinterfaceObj.pk)
        self.message_user(request, f"{queryset.count()} tasks queued.")
    start_pull_places_task.short_description = "Pull places data for selected interfaces."
