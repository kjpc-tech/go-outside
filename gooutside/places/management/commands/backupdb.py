from django.core.management.base import BaseCommand
from django.conf import settings

import os
import re
import subprocess

from operator import itemgetter

BACKUP_MAX_AMOUNT = 5

# requires .pgpass file to be in use


class Command(BaseCommand):
    help = 'Backs up database.'

    def add_arguments(self, parser):
        parser.add_argument('destination', help='Destination path to store sql files.')
        parser.add_argument('--verbose', action='store_true', default=False, help='Print verbose output.')

    def handle(self, *args, **options):
        if options['verbose']:
            self.stdout.write(self.style.SUCCESS('\nDatabase backup: starting.\n'))

        if options['destination'] is not None:
            if os.path.exists(options['destination']):
                self.backup(options['destination'])
            else:
                self.stdout.write(self.style.ERROR("\tInvalid destination path."))

        if options['verbose']:
            self.stdout.write(self.style.SUCCESS('\nDatabase backup: done.\n'))

    def backup(self, directory_save):
        # rename / delete old backups
        need_moved = []
        for filename in os.listdir(directory_save):
            match = re.match('gooutside_database.(\d+).sql', filename)
            if match is not None:
                need_moved.append((filename, int(match.group(1))))

        if len(need_moved) > 0:
            need_moved = list(reversed(sorted(need_moved, key=itemgetter(1))))
            # only store a certain number of backups
            if need_moved[0][1] >= BACKUP_MAX_AMOUNT:
                # delete old backup
                os.remove(os.path.join(directory_save, need_moved[0][0]))
                need_moved.pop(0)

            # need_moved is sorted so this works
            for n in need_moved:
                # rename with incremented number
                os.rename(os.path.join(directory_save, n[0]), os.path.join(directory_save, "gooutside_database.{}.sql".format(n[1] + 1)))

        # move the most current backup into number 1
        if os.path.exists(os.path.join(directory_save, "gooutside_database.sql")):
            os.rename(os.path.join(directory_save, "gooutside_database.sql"), os.path.join(directory_save, "gooutside_database.1.sql"))

        if not settings.DEBUG:  # assume we have pg_dump
            database_name = settings.DATABASES['default']['NAME']
            database_user = settings.DATABASES['default']['USER']

            dump_command = [
                "pg_dump",
                "--username={}".format(database_user),
                "-w",  # use .pgpass file for password
                "--file={}".format(os.path.join(directory_save, "gooutside_database.sql")),
                "{}".format(database_name),
            ]
            subprocess.Popen(dump_command)
