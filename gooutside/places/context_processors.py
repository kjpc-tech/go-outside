from django.conf import settings


def places(request):
    return {
        'google_analytics': getattr(settings, 'GOOGLE_ANALYTICS', None)
    }
