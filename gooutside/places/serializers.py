from rest_framework import serializers

from .models import PlaceCategory, PlaceAttribute, PlaceSource, Place


class PlaceCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = PlaceCategory
        fields = ('id', 'name')


class PlaceAttributeSerializer(serializers.ModelSerializer):
    class Meta:
        model = PlaceAttribute
        fields = ('id', 'name')


class PlaceSourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = PlaceSource
        fields = ('id', 'name')


class PlaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Place
        fields = ('id', 'name', 'category', 'location', 'state', 'attributes', 'source', 'source_identification')

    def to_representation(self, obj):
        """
        Override to_representation to get better 'location' output.

        TODO is there a better way to do this?
        """
        point_representation = {
            'lat': obj.location.y,
            'lng': obj.location.x,
        }

        representation = super().to_representation(obj)

        representation['location'] = point_representation

        return representation
