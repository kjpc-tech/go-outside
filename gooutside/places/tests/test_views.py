import json

from django.test import TestCase
from django.urls import reverse
from django.contrib.gis.geos import Point

from places.models import (
    PlaceCategory, PlaceAttribute, PlaceSource, Place,
)


class PlacesViewTests(TestCase):
    def test_index_url_routes_here(self):
        response = self.client.get('/', secure=True)

        self.assertEqual(response.status_code, 200)

        self.assertTemplateUsed(response, 'places/places.html')

    def test_template_and_context(self):
        response = self.client.get(reverse('places:places'), secure=True)

        self.assertEqual(response.status_code, 200)

        self.assertTemplateUsed(response, 'places/places.html')

        self.assertIn('states', response.context.keys())
        self.assertIn('google_maps_api_key', response.context.keys())


class PlaceCategoryListViewTests(TestCase):
    def test_response_with_no_data(self):
        response = self.client.get(reverse('places:category_list'), secure=True)

        self.assertEqual(response.status_code, 200)

        categories = json.loads(response.content)

        self.assertEqual(len(categories), 0)

    def test_response_with_data(self):
        PlaceCategory.objects.create(
            name="Test Category",
        )

        response = self.client.get(reverse('places:category_list'), secure=True)

        self.assertEqual(response.status_code, 200)

        categories = json.loads(response.content)

        self.assertEqual(len(categories), 1)

        self.assertEqual(categories[0]['name'], 'Test Category')


class PlaceAttributeListViewTests(TestCase):
    def test_response_with_no_data(self):
        response = self.client.get(reverse('places:attribute_list'), secure=True)

        self.assertEqual(response.status_code, 200)

        attributes = json.loads(response.content)

        self.assertEqual(len(attributes), 0)

    def test_response_with_data(self):
        PlaceAttribute.objects.create(
            name="Test Attribute",
        )

        response = self.client.get(reverse('places:attribute_list'), secure=True)

        self.assertEqual(response.status_code, 200)

        attributes = json.loads(response.content)

        self.assertEqual(len(attributes), 1)

        self.assertEqual(attributes[0]['name'], 'Test Attribute')


class PlaceSourceListViewTests(TestCase):
    def test_response_with_no_data(self):
        response = self.client.get(reverse('places:source_list'), secure=True)

        self.assertEqual(response.status_code, 200)

        sources = json.loads(response.content)

        self.assertEqual(len(sources), 0)

    def test_response_with_data(self):
        PlaceSource.objects.create(
            name="Test Source",
        )

        response = self.client.get(reverse('places:source_list'), secure=True)

        self.assertEqual(response.status_code, 200)

        sources = json.loads(response.content)

        self.assertEqual(len(sources), 1)

        self.assertEqual(sources[0]['name'], 'Test Source')


class PlaceListViewTests(TestCase):
    def setUp(self):
        self.category1 = PlaceCategory.objects.create(name="Test Category 1")
        self.category2 = PlaceCategory.objects.create(name="Test Category 2")
        self.attribute1 = PlaceAttribute.objects.create(name="Test Attribute 1")
        self.attribute2 = PlaceAttribute.objects.create(name="Test Attribute 2")
        self.source1 = PlaceSource.objects.create(name="Test Source 1")
        self.source2 = PlaceSource.objects.create(name="Test Source 2")

    def test_response_with_no_data(self):
        response = self.client.get(reverse('places:place_list'), secure=True)

        self.assertEqual(response.status_code, 200)

        places = json.loads(response.content)

        self.assertEqual(len(places), 0)

    def test_response_with_data(self):
        place1 = Place.objects.create(
            name="Test Place 1",
            category=self.category1,
            location=Point(x=-106.5307, y=47.8889, srid=4326),
            state='MT',
            source=self.source1,
        )
        place1.attributes.add(self.attribute1)

        place2 = Place.objects.create(
            name="Test Place 2",
            category=self.category2,
            location=Point(x=-108.1941, y=43.3039, srid=4326),
            state='WY',
            source=self.source2,
        )
        place2.attributes.add(self.attribute2)

        response = self.client.get(reverse('places:place_list'), secure=True)

        self.assertEqual(response.status_code, 200)

        places = json.loads(response.content)

        self.assertEqual(len(places), 2)

        self.assertEqual(places[0]['name'], 'Test Place 1')
        self.assertEqual(places[1]['name'], 'Test Place 2')

        response = self.client.get(reverse('places:place_list'), {
            'category': self.category1.pk,
        }, secure=True)

        self.assertEqual(response.status_code, 200)

        places = json.loads(response.content)

        self.assertEqual(len(places), 1)

        self.assertEqual(places[0]['name'], 'Test Place 1')

        response = self.client.get(reverse('places:place_list'), {
            'category': self.category2.pk,
        }, secure=True)

        self.assertEqual(response.status_code, 200)

        places = json.loads(response.content)

        self.assertEqual(len(places), 1)

        self.assertEqual(places[0]['name'], 'Test Place 2')

        response = self.client.get(reverse('places:place_list'), {
            'attributes[]': [self.attribute1.pk],
        }, secure=True)

        self.assertEqual(response.status_code, 200)

        places = json.loads(response.content)

        self.assertEqual(len(places), 1)

        self.assertEqual(places[0]['name'], 'Test Place 1')

        response = self.client.get(reverse('places:place_list'), {
            'attributes[]': [self.attribute2.pk],
        }, secure=True)

        self.assertEqual(response.status_code, 200)

        places = json.loads(response.content)

        self.assertEqual(len(places), 1)

        self.assertEqual(places[0]['name'], 'Test Place 2')

        response = self.client.get(reverse('places:place_list'), {
            'attributes[]': [self.attribute1.pk, self.attribute2.pk],
        }, secure=True)

        self.assertEqual(response.status_code, 200)

        places = json.loads(response.content)

        self.assertEqual(len(places), 2)

        response = self.client.get(reverse('places:place_list'), {
            'state': 'MT',
        }, secure=True)

        self.assertEqual(response.status_code, 200)

        places = json.loads(response.content)

        self.assertEqual(len(places), 1)

        self.assertEqual(places[0]['name'], 'Test Place 1')

        response = self.client.get(reverse('places:place_list'), {
            'state': 'WY',
        }, secure=True)

        self.assertEqual(response.status_code, 200)

        places = json.loads(response.content)

        self.assertEqual(len(places), 1)

        self.assertEqual(places[0]['name'], 'Test Place 2')

        response = self.client.get(reverse('places:place_list'), {
            'search': '1',
        }, secure=True)

        self.assertEqual(response.status_code, 200)

        places = json.loads(response.content)

        self.assertEqual(len(places), 1)

        self.assertEqual(places[0]['name'], 'Test Place 1')

        response = self.client.get(reverse('places:place_list'), {
            'search': '2',
        }, secure=True)

        self.assertEqual(response.status_code, 200)

        places = json.loads(response.content)

        self.assertEqual(len(places), 1)

        self.assertEqual(places[0]['name'], 'Test Place 2')

        response = self.client.get(reverse('places:place_list'), {
            'category': self.category1.pk,
            'attributes[]': [self.attribute1.pk],
            'state': 'MT',
        }, secure=True)

        self.assertEqual(response.status_code, 200)

        places = json.loads(response.content)

        self.assertEqual(len(places), 1)

        self.assertEqual(places[0]['name'], 'Test Place 1')

        response = self.client.get(reverse('places:place_list'), {
            'category': self.category1.pk,
            'state': 'WY',
        }, secure=True)

        self.assertEqual(response.status_code, 200)

        places = json.loads(response.content)

        self.assertEqual(len(places), 0)

        response = self.client.get(reverse('places:place_list'), {
            'category': self.category1.pk,
            'attributes[]': [self.attribute1.pk],
            'state': 'WY',
        }, secure=True)

        self.assertEqual(response.status_code, 200)

        places = json.loads(response.content)

        self.assertEqual(len(places), 0)

        response = self.client.get(reverse('places:place_list'), {
            'category': self.category2.pk,
            'attributes[]': [self.attribute2.pk],
            'state': 'WY',
            'search': '2',
        }, secure=True)

        self.assertEqual(response.status_code, 200)

        places = json.loads(response.content)

        self.assertEqual(len(places), 1)

        self.assertEqual(places[0]['name'], 'Test Place 2')
