from django.test import TestCase

from places.models import (
    PlaceCategory, PlaceAttribute, PlaceSource, Place,
    PlaceSourceInterfaceMapping, PlaceSourceInterface,
)


class PlaceCategoryModelTests(TestCase):
    pass


class PlaceAttributeModelTests(TestCase):
    pass


class PlaceSourceModelTests(TestCase):
    pass


class PlaceModelTests(TestCase):
    pass


class PlaceSourceInterfaceMappingModelTests(TestCase):
    def test_get_key_str_method(self):
        placesourceinterfacemappingObj = PlaceSourceInterfaceMapping.objects.create(
            key='a',
            rule='x',
            success='',
        )

        self.assertEqual(placesourceinterfacemappingObj.get_key_str(), "['a']")

        placesourceinterfacemappingObj = PlaceSourceInterfaceMapping.objects.create(
            key='a__b',
            rule='x',
            success='',
        )

        self.assertEqual(placesourceinterfacemappingObj.get_key_str(), "['a']['b']")

        placesourceinterfacemappingObj = PlaceSourceInterfaceMapping.objects.create(
            key='a__b_c',
            rule='x',
            success='',
        )

        self.assertEqual(placesourceinterfacemappingObj.get_key_str(), "['a']['b_c']")

    def test_get_rule_method(self):
        placesourceinterfacemappingObj = PlaceSourceInterfaceMapping.objects.create(
            key='a',
            rule='x',
            success='',
        )

        # should return raw value
        self.assertEqual(placesourceinterfacemappingObj.get_rule()('value'), 'value')

        placesourceinterfacemappingObj = PlaceSourceInterfaceMapping.objects.create(
            key='a',
            rule='x[:2]',
            success='',
        )

        # should return first two chars from raw value
        self.assertEqual(placesourceinterfacemappingObj.get_rule()('value'), 'va')

        placesourceinterfacemappingObj = PlaceSourceInterfaceMapping.objects.create(
            key='a',
            rule='x == "value"',
            success='',
        )

        # should return True
        self.assertEqual(placesourceinterfacemappingObj.get_rule()('value'), True)

    def test_evaluate_mapping_method(self):
        json_object = {
            'a': "value",
        }

        placesourceinterfacemappingObj = PlaceSourceInterfaceMapping.objects.create(
            key='a',
            rule='x',
            success='',
        )

        # should return 'value'
        self.assertEqual(placesourceinterfacemappingObj.evaluate_mapping(json_object), 'value')

        placesourceinterfacemappingObj = PlaceSourceInterfaceMapping.objects.create(
            key='a',
            rule='x[:2]',
            success='',
        )

        # should return 'va'
        self.assertEqual(placesourceinterfacemappingObj.evaluate_mapping(json_object), 'va')

        placesourceinterfacemappingObj = PlaceSourceInterfaceMapping.objects.create(
            key='a',
            rule='x == "value"',
            success='Matched',
        )

        # should return 'Matched'
        self.assertEqual(placesourceinterfacemappingObj.evaluate_mapping(json_object), 'Matched')


class PlaceSourceInterfaceModelTests(TestCase):
    pass
