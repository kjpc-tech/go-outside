import time

from django.urls import reverse
from django.contrib.gis.geos import Point

from gooutside.tests.tests import MySeleniumTestCase

from places.models import (
    PlaceCategory, PlaceAttribute, PlaceSource, Place,
)


class PlacesFlowTests(MySeleniumTestCase):
    def setUp(self):
        self.category1 = PlaceCategory.objects.create(name="Test Category 1")
        self.category2 = PlaceCategory.objects.create(name="Test Category 2")

        self.attribute1 = PlaceAttribute.objects.create(name="Test Attribute 1")
        self.attribute2 = PlaceAttribute.objects.create(name="Test Attribute 2")

        self.source1 = PlaceSource.objects.create(name="Test Source 1")
        self.source2 = PlaceSource.objects.create(name="Test Source 2")

        self.place1 = Place.objects.create(
            name="Test Place 1",
            category=self.category1,
            location=Point(x=-106.5307, y=47.8889, srid=4326),
            state='MT',
            source=self.source1,
        )
        self.place1.attributes.add(self.attribute1)

        self.place2 = Place.objects.create(
            name="Test Place 2",
            category=self.category2,
            location=Point(x=-108.1941, y=43.3039, srid=4326),
            state='WY',
            source=self.source2,
        )
        self.place2.attributes.add(self.attribute2)

    def test_places_renders(self):
        self.selenium.get("{}{}".format(self.live_server_url, reverse('places:places')))

        self.assertIn("Go Outside", self.selenium.page_source)

        time.sleep(0.5)

        # confirm that both markers are on map
        self.assertElementDoesExist('area[title="Test Place 1"]')
        self.assertElementDoesExist('area[title="Test Place 2"]')

        # select category
        self.choose_select_option('category', self.category1.pk)

        time.sleep(0.5)

        # confirm only one marker on map
        self.assertElementDoesExist('area[title="Test Place 1"]')
        self.assertElementDoesntExist('area[title="Test Place 2"]')

        self.choose_select_option('category', 'all')

        # select attribute
        self.selenium.find_element_by_css_selector('input[name="attributes"][value="{}"]'.format(self.attribute1.pk)).click()

        time.sleep(0.5)

        # confirm only one marker on map
        self.assertElementDoesExist('area[title="Test Place 1"]')
        self.assertElementDoesntExist('area[title="Test Place 2"]')

        self.selenium.find_element_by_css_selector('input[name="attributes"][value="{}"]'.format(self.attribute1.pk)).click()

        # select state
        self.choose_select_option('state', 'MT')

        time.sleep(0.5)

        # confirm only one marker on map
        self.assertElementDoesExist('area[title="Test Place 1"]')
        self.assertElementDoesntExist('area[title="Test Place 2"]')

        self.choose_select_option('state', 'all')

        # search
        self.update_input_value('search', "Test Place 1")
        self.selenium.find_element_by_css_selector("#map").click()

        time.sleep(0.5)

        # confirm only one marker on map
        self.assertElementDoesExist('area[title="Test Place 1"]')
        self.assertElementDoesntExist('area[title="Test Place 2"]')

        self.clear_input_with_backspace('search')
        self.selenium.find_element_by_css_selector("#map").click()

        time.sleep(0.5)

        # confirm that both markers are on map
        self.assertElementDoesExist('area[title="Test Place 1"]')
        self.assertElementDoesExist('area[title="Test Place 2"]')

        # set all filters
        self.choose_select_option('category', self.category2.pk)
        self.selenium.find_element_by_css_selector('input[name="attributes"][value="{}"]'.format(self.attribute2.pk)).click()
        self.choose_select_option('state', 'WY')

        time.sleep(0.5)

        # confirm only one marker on map
        self.assertElementDoesntExist('area[title="Test Place 1"]')
        self.assertElementDoesExist('area[title="Test Place 2"]')

        # add search
        self.update_input_value('search', "2")
        self.selenium.find_element_by_css_selector("#map").click()

        time.sleep(0.5)

        # confirm only one marker on map
        self.assertElementDoesntExist('area[title="Test Place 1"]')
        self.assertElementDoesExist('area[title="Test Place 2"]')

        self.clear_input_with_backspace('search')
        self.selenium.find_element_by_css_selector("#map").click()

        # set filter to include no places
        self.choose_select_option('state', 'MT')

        time.sleep(0.5)

        # confirm no markers on map
        self.assertElementDoesntExist('area[title="Test Place 1"]')
        self.assertElementDoesntExist('area[title="Test Place 2"]')
